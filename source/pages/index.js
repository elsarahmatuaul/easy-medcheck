//Import dan Export halaman aplikasi
import Home from './Home'
import Klinik from './Klinik'
import MedCheck from './MedCheck'
import MedCheckResult from './MedCheckResult'
import News from './News'
import Splash from './Splash'

export {
    Home,
    Klinik,
    MedCheck,
    MedCheckResult,
    News,
    Splash
}