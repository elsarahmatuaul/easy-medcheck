// Import dan Export icon
import HomeIcon from './home.svg'
import HomeActiveIcon from './homeActive.svg'
import CheckIcon from './check.svg'
import CheckActiveIcon from './checkActive.svg'
import KlinikIcon from './klinik.svg'
import KlinikActiveIcon from './klinikActive.svg'

export { HomeIcon, HomeActiveIcon, CheckIcon, CheckActiveIcon, KlinikIcon, KlinikActiveIcon }